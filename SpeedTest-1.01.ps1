﻿$output = "$env:TEMP\out.dat"
$TestDate = date -Format 'yyyymmdd-hhmmss'
$LogFile = "$PSScriptRoot\Test-Log.csv"

$URIS = @(
    "http://mirror.internode.on.net/pub/test/100meg.test"
    "http://ftp.iinet.net.au/test100MB.dat"
    "http://speedtest.education.netspace.net.au/ram/100mb.dat"
    "http://mirror.aarnet.edu.au/pub/raspbian/images/apc_apricot_r2.1.zip"
    "http://www.tpg.com.au/test3.iso"
    #"http://www.tpg.com.au/test4.iso"
    "http://mirror.filearena.net/pub/speed/SpeedTest_128MB.dat"
    "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/jquery-speedtest/100MB.txt"
    "http://au.download.windowsupdate.com/d/msdownload/update/driver/drvs/2017/01/200038370_d27c6d76a6cc8789ad9dbbd5a02ddb836b9a2b75.cab"
    "http://support.apple.com/downloads/DL1858/en_US/osxupd10.11.3.dmg"
    #http://www.softlayer.com/data-centers
    "http://speedtest.ams01.softlayer.com/downloads/test100.zip"
    "http://speedtest.ams03.softlayer.com/downloads/test100.zip"
    "http://speedtest.che01.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal01.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal05.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal06.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal07.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal09.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal10.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal12.softlayer.com/downloads/test100.zip"
    "http://speedtest.dal13.softlayer.com/downloads/test100.zip"
    "http://speedtest.fra02.softlayer.com/downloads/test100.zip"
    "http://speedtest.hkg02.softlayer.com/downloads/test100.zip"
    "http://speedtest.hou02.softlayer.com/downloads/test100.zip"
    "http://speedtest.lon02.softlayer.com/downloads/test100.zip"
    "http://speedtest.mel01.softlayer.com/downloads/test100.zip"
    "http://speedtest.mil01.softlayer.com/downloads/test100.zip"
    "http://speedtest.mon01.softlayer.com/downloads/test100.zip"
    "http://speedtest.osl01.softlayer.com/downloads/test100.zip"
    "http://speedtest.par01.softlayer.com/downloads/test100.zip"
    "http://speedtest.mex01.softlayer.com/downloads/test100.zip"
    "http://speedtest.sjc01.softlayer.com/downloads/test100.zip"
    "http://speedtest.sjc03.softlayer.com/downloads/test100.zip"
    "http://speedtest.sao01.softlayer.com/downloads/test100.zip"
    "http://speedtest.sea01.softlayer.com/downloads/test100.zip"
    "http://speedtest.sng01.softlayer.com/downloads/test100.zip"
    "http://speedtest.syd01.softlayer.com/downloads/test100.zip"
    "http://speedtest.tok02.softlayer.com/downloads/test100.zip"
    "http://speedtest.tor01.softlayer.com/downloads/test100.zip"
    "http://speedtest.wdc01.softlayer.com/downloads/test100.zip"
    "http://speedtest.wdc04.softlayer.com/downloads/test100.zip"
)

foreach ($URI in $URIS){
    Write-Host "Starting Download from $URI"
    $Speed = Measure-Command {
        #Start-BitsTransfer -Source $URI -Destination $output
        (New-Object System.Net.WebClient).DownloadFile($URI, $output)
    }

    $TestMetaData = Test-NetConnection -TraceRoute -InformationLevel Detailed -ComputerName $([System.Uri]$URI).Host

    $File = Get-ChildItem $output

    $CalcSpeed = ($File.Length / $Speed.TotalSeconds)/ 1mb

    $OutputData = [PSCustomObject]@{}
    $OutputData| Add-Member -Name "Date" -Value $TestDate -MemberType NoteProperty
    $OutputData| Add-Member -Name "URI" -Value $URI -MemberType NoteProperty
    $OutputData| Add-Member -Name "TotalSeconds" -Value $($Speed.TotalSeconds) -MemberType NoteProperty
    $OutputData| Add-Member -Name "MB/s" -Value $CalcSpeed -MemberType NoteProperty
    
    $OutputData| Add-Member -Name "RemoteAddress" -Value $TestMetaData.RemoteAddress -MemberType NoteProperty
    
    $OutputData| Add-Member -Name "SourceAddress (IPAddress)" -Value $TestMetaData.SourceAddress.IPAddress -MemberType NoteProperty
    $OutputData| Add-Member -Name "SourceAddress (PrefixOrigin)" -Value $TestMetaData.SourceAddress.PrefixOrigin -MemberType NoteProperty
    $OutputData| Add-Member -Name "SourceAddress (InterfaceAlias)" -Value $TestMetaData.SourceAddress.InterfaceAlias -MemberType NoteProperty
    
    $OutputData| Add-Member -Name "NetRoute (NextHop)" -Value $TestMetaData.NetRoute.NextHop -MemberType NoteProperty
    $OutputData| Add-Member -Name "PingReplyDetails (RTT)" -Value $TestMetaData.PingReplyDetails.RoundtripTime -MemberType NoteProperty
    $OutputData| Add-Member -Name "TraceRoute (Length)" -Value $TestMetaData.TraceRoute.Length -MemberType NoteProperty

    $Count = 0
    0..30 |ForEach-Object {
       $OutputData| Add-Member -Name "TraceRoute ($Count)" -Value $TestMetaData.TraceRoute[$Count] -MemberType NoteProperty
       $Count += 1
    }

    Write-Host "File Downloaded in $($Speed.TotalSeconds) seconds at $CalcSpeed mb/s from $URI"

    $OutputData | Export-Csv -Path $LogFile -NoTypeInformation -Append
}